Eryka Nosal
MPCS53013, Autumn 2016
HW6

-------

HW6-1: WeatherIngest
This program serializes weather data into thrift, flight data from zipped CSV files

Folders
	/txt_output - has the output of me running my scripts. Shows the average departure delay for routes grouped by (origin, dest) for each weather condition
	/data - has raw weather data and raw flight data
	/pig_scripts 

To run:
	1. sh hw61_runsetup.sh
		This cleans up folders on HDFS and also ingests flight data
	2. pass /home/mpcs53013/workspace/WeatherFlightIngest/data/weatherData as the argument for SerializeWeatherSummary.java
	3. sh hw61_runpig.sh
		Runs all the pig scripts including those that get the average departure delay per weather condition for each route

Output: 
	See txt_output folder or 	/outputs/flights_and_weather/hw61_***_pigresults on HDFS

-----


HW6-3: WeatherFlightIngest
This program serializes flight and weather data into thrift

Folders
	Same as HW61

To run:
	
	1. sh hw63_runsetup.sh
	2. pass /home/mpcs53013/workspace/WeatherFlightIngest/data/weatherData as the argument for SerializeWeatherSummary.java
	3. pass /home/mpcs53013/workspace/WeatherFlightIngest/data/flightData as the argument for SerializeFlightSummary.java
	4. sh hw63_runpig.sh


Output
	Flight data is written out to HDFS /inputs/thriftFlight/flight-YYYY-MM



















-----ETC
My 6-1 notes:

start-dfs.sh
start-yarn.sh
hdfs dfs -mkdir /inputs/weathergeo
hdfs dfs -copyFromLocal stationcodes.txt /inputs/weathergeo/stationcodes.txt
getWeather.sh
untarWeather.sh

run WeatherIngest with /home/mpcs53013/workspace/hw6_lecture_files/weatherData as argument

getFlights.sh
ingestFlights.sh
readWeather.pig
step1.pig
	IN
	/inputs/airline/On_Time_On_Time_Performance_*
	/inputs/weathergeo/station_codes.txt

	OUT
	/inputs/flight_stations

step2.pig
	IN
	/inputs/flight_stations

	OUT
	/inputs/flights_and_weather

hw6_1.pig
	IN
	/inputs/flights_and_weather

	OUT
	/outputs/flights_and_weather/hw61_***_pigresults
		where *** is fog,rain,snow,hail,hunder,tornado,clear


