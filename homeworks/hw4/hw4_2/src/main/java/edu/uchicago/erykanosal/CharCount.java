package edu.uchicago.erykanosal;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
// Taken from https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html#Source_Code
public class CharCount {

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
    	//Split into characters, put in array
    	char[] charArr = value.toString().toCharArray();
      
    	Pattern punct = Pattern.compile("\\p{Punct}");
    	Matcher m;
    	
    	int i = 0;
    	while (i < charArr.length) {
    		System.out.println("i: " + i);
    		if (Character.isUpperCase(charArr[i])) 
    			word.set("Uppercase");
    		else if (Character.isLowerCase(charArr[i])) 
    			word.set("Lowercase");
    		else if (Character.isDigit(charArr[i]))
    			word.set("Digit");
    		else if (Character.isWhitespace(charArr[i]))
    			word.set("Whitespace");
    		else {
    			m = punct.matcher(String.valueOf(charArr[i]));
    			if (m.matches())
    				word.set("Punctuation");
    			else {
    				word.set("Other");
    			} //end inner if/else
    		} //end outer if/else
    		context.write(word, one);
    		i++;
    	} //end while
	} //end map() method
    
  } //end TokenizerMapper class

  
  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    conf.addResource(new Path("/home/mpcs53013/hadoop/etc/hadoop/core-site.xml"));
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(CharCount.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}