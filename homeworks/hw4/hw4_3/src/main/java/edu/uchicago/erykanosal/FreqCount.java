package edu.uchicago.erykanosal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.map.InverseMapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
// Taken from https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html#Source_Code
public class FreqCount {

	//MAPPER1
	public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable>{
		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			//Split on: one or more spaces, zero or more punctuation
			String[] wordParts = value.toString().split("\\s+(\\p{Punct})*");
   	
			//Alpha character (lower and upper)
			Pattern punct = Pattern.compile("^\\p{Alpha}+");
			Matcher m;
   	
			int i = 0;    	
			while (i < wordParts.length) {
				//Remove one or more punctuation at the end
				wordParts[i] = wordParts[i].replaceAll("$\\p{Punct}*", "");

				//Only accept the word if it starts with an alpha character (lower or upper)
				m = punct.matcher(wordParts[i]);
				if (m.matches()) {
					//Combine upper and lowercase
					word.set(wordParts[i].toLowerCase());
					context.write(word, one);
				} //end if m.matches() 
				i++;
			}// end while loop
		} // end map() method
	} //end Mapper class

	
	//REDUCER1
	public static class IntSumReducer extends Reducer<Text,IntWritable,Text,IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}


	//MAPPER2
	public static class InverseMapper extends Mapper<Object, Text, IntWritable, Text>{
		private Text word = new Text();
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			//Split on new line
			String[] lineInputs = value.toString().split("\\n");
   	   		System.out.println("IN MAPPER 2");
			
			int i = 0;    	
			while (i < lineInputs.length) {
				//Split on tab
				String[] lineParts = lineInputs[i].split("\\t"); 

				word.set(lineParts[0]);
				context.write(new IntWritable(Integer.parseInt(lineParts[1])), word); 
				i++;
			}// end while loop
		} // end map() method
	} //end Mapper class
	
	public static class ListReducer extends Reducer<IntWritable,Text,IntWritable,Text> {
		public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			System.out.println("IN REDUCER2");
			List<String> wordList = new ArrayList<>();
			for (Text word : values) {
				wordList.add(word.toString());
				System.out.println("word " + word);
			}
			
			//Convert from arraylist<String> to String[]
			String[] wordArray = wordList.toArray(new String[wordList.size()]);
			
			//Put into Text
			Text result = new Text();
			result.set(Arrays.toString(wordArray));
			
			//Write out key-values
			context.write(key, result);
		}
	}

	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		conf.addResource(new Path("/home/mpcs53013/hadoop/etc/hadoop/core-site.xml"));
		Job job = Job.getInstance(conf, "word count");
		job.setMapperClass(TokenizerMapper.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		boolean success = job.waitForCompletion(true);
		if (success) {
			System.out.println("IN IF");
			Job job2 = Job.getInstance(conf, "freq count");
			job2.setMapperClass(InverseMapper.class);
			job2.setReducerClass(ListReducer.class);
			job2.setOutputKeyClass(IntWritable.class);
			job2.setOutputValueClass(Text.class);
			FileInputFormat.addInputPath(job2, new Path(args[1]));
			FileOutputFormat.setOutputPath(job2, new Path(args[2]));
			System.out.println("END OF IF");
			success = job2.waitForCompletion(true);
		}
		System.exit(success ? 0 : 1);
  }
	
}