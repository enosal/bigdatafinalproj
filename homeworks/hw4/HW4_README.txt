Eryka Nosal
MPCS53013, Autumn 2016
HW4

-------

HW4-1: 
VM: I ran each three times and took the system timings (in ms). The combiner looks as if it increases the speed of the MapReduce computations
	VM, no combiner: 33681, 30483, 31485
	VM, combiner: 22798, 25518, 25495

GCLOUD: I ran each just once with timings (in ms) as well as performance information. The timing also decreased with the combiner as opposed to without it
	Cluster, no combiner: 54177 
	Job Counters 
		Launched map tasks=2
		Launched reduce tasks=1
		Rack-local map tasks=2
		Total time spent by all maps in occupied slots (ms)=37712
		Total time spent by all reduces in occupied slots (ms)=44064
		Total time spent by all map tasks (ms)=37712
		Total time spent by all reduce tasks (ms)=22032
		Total vcore-seconds taken by all map tasks=37712
		Total vcore-seconds taken by all reduce tasks=22032
		Total megabyte-seconds taken by all map tasks=57925632
		Total megabyte-seconds taken by all reduce tasks=67682304
	Map-Reduce Framework
		Map input records=2352368
		Map output records=10243812
		Map output bytes=290646362
		Map output materialized bytes=311661058
		Input split bytes=264
		Combine input records=0
		Combine output records=0
		Reduce input groups=1266890
		Reduce shuffle bytes=311661058
		Reduce input records=10243812
		Reduce output records=1266890
		Spilled Records=20487624
		Shuffled Maps =2
		Failed Shuffles=0
		Merged Map outputs=2
		GC time elapsed (ms)=527
		CPU time spent (ms)=55580
		Physical memory (bytes) snapshot=2812588032
		Virtual memory (bytes) snapshot=7856463872
		Total committed heap usage (bytes)=2854748160
	File Input Format Counters 
		Bytes Read=227108725
	File Output Format Counters 
		Bytes Written=96338386
	Total execution time: 54177


	Cluster, combiner: 43754
	Job Counters 
		Launched map tasks=2
		Launched reduce tasks=1
		Data-local map tasks=2
		Total time spent by all maps in occupied slots (ms)=43060
		Total time spent by all reduces in occupied slots (ms)=17858
		Total time spent by all map tasks (ms)=43060
		Total time spent by all reduce tasks (ms)=8929
		Total vcore-seconds taken by all map tasks=43060
		Total vcore-seconds taken by all reduce tasks=8929
		Total megabyte-seconds taken by all map tasks=66140160
		Total megabyte-seconds taken by all reduce tasks=27429888
	Map-Reduce Framework
		Map input records=2352368
		Map output records=10243812
		Map output bytes=290646362
		Map output materialized bytes=160367294
		Input split bytes=264
		Combine input records=10243812
		Combine output records=1851963
		Reduce input groups=1266890
		Reduce shuffle bytes=160367294
		Reduce input records=1851963
		Reduce output records=1266890
		Spilled Records=3703926
		Shuffled Maps =2
		Failed Shuffles=0
		Merged Map outputs=2
		GC time elapsed (ms)=962
		CPU time spent (ms)=46420
		Physical memory (bytes) snapshot=2912198656
		Virtual memory (bytes) snapshot=7858634752
		Total committed heap usage (bytes)=2995257344
	File Input Format Counters 
		Bytes Read=227108725
	File Output Format Counters 
		Bytes Written=96338386
	Total execution time: 43754




HW4-2: Character grouping type of the complete works of Shakespeare
	I grouped characters by: lowercase, uppercase, digit, whitespace, punctuation. Anything that didn't fall under those categories was put into "Other".
	The results are in hw42_results.txt

		Digit	3355
		Lowercase	3298003
		Punctuation	246202
		Uppercase	506868
		Whitespace	1285884

HW4-3 (EC): Two map-reduce jobs are used to accomplish this task. The results are in hw43_results.text
	Three arguments are needed to run HW4-3. (inputDir tempDir outputDir). tempDir is the output of mapreduce1 and the input to mapreduce2
	Mapper 1: Reads in document, parses the words using HW4-4. 
	Reducer 1: Outputs each word as a Key, and the summed occurence of that word as a value

	Mapper 2: Reads in the temporary output from MapReduce. Writes out the occurence as a key, the word as the value
	Reducer 2: Goes through each key (number of occurences) and creates a list of all the words for that key. The list is tranformed into a string to put into Text.


HW4-4 (EC): Three steps to creating an ImprovedWordCount program. The results are in hw44_results.txt
	1. Split the incoming text on: one or more spaces followed by zero or more punctuation
	2. Remove one or more punctuation at the end
	3. Only add the word to the context if it starts with an alpha character (this removes all numbers entries)

	I believe that contractions are incorrectly split, however