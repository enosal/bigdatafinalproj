package edu.uchicago.erykanosal;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskReport;
import org.apache.hadoop.mapreduce.TaskType;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
// Taken from https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html#Source_Code
public class WordCountCombiner {

  public static void main(String[] args) throws Exception {
	System.out.println("W/ combiner");
	final long startTime = System.currentTimeMillis();    
	  
	Configuration conf = new Configuration();
    conf.addResource(new Path("/home/mpcs53013/hadoop/etc/hadoop/core-site.xml"));
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(WordCountCombiner.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    
    job.waitForCompletion(true);

    final long endTime = System.currentTimeMillis();
    
    //FOR VM - uncomment out
    System.out.println("Total execution time: " + (endTime - startTime));
    //END FOR VM
    
    
    //FOR CLUSTER - comment out on VM
    FileSystem fs=FileSystem.get(conf);
    Path pt = new Path("/tmp/erykanosal_wordcountcombiner_hw41.txt");
    BufferedWriter br=new BufferedWriter(new OutputStreamWriter(fs.create(pt,true)));
    // TO append data to a file, use fs.append(Path f)
    br.write("WORDCOUNT");
    br.write("Total execution time: " + (endTime - startTime));
    TaskReport[] reportsMap = job.getTaskReports(TaskType.MAP);
    for(TaskReport report : reportsMap) { 
    	long time = report.getFinishTime() - report.getStartTime();
    	br.write(report.getTaskId() + " took " + time + " millis!");
    }
    TaskReport[] reportsRed = job.getTaskReports(TaskType.REDUCE);
    for(TaskReport report : reportsRed) { 
    	long time = report.getFinishTime() - report.getStartTime();
    	br.write(report.getTaskId() + " took " + time + " millis!");
    }


    br.close();
    //END FOR CLUSTER

    
    
  }
  
  
	
  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
        word.set(itr.nextToken());
        context.write(word, one);
      }
    }
  }


  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }

}