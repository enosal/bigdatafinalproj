Eryka Nosal
MPCS53013, Autumn 2016
HW7

-------

HW7-1: Input origin airport for weather delays
	hw71-route-report.pl
	hw71_EN_origin_delays.pig
	hw71-origin-delays.html



HW7-2: Input origin and destination for weather delays (by carrier)
	hw72-route-report.pl
	hw72_EN_carrier_delays.pig
	hw72-carrier-delays.html
	





---my Notes
start-dfs.sh
start-yarn.sh
start-hbase.sh
weather_delays.pig

sudo service apache2 start

hbase rest server

hbase shell to create EN_weather_delays_by_origin table
hbase shell to create EN_route_delays_by_carrier table