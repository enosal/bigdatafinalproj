package edu.uchicago.erykanosal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.map.InverseMapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
// Taken from https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html#Source_Code
public class LengthFreq {

	public static class TokenizerMapper extends Mapper<Object, Text, IntWritable, IntWritable>{
		private Text word = new Text();
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			//Split on: one or more spaces, zero or more punctuation
			String[] wordParts = value.toString().split("\\s+(\\p{Punct})*");
   	
			//Alpha character (lower and upper)
			Pattern punct = Pattern.compile("^\\p{Alpha}+");
			Matcher m;
   	
			int i = 0;    	
			while (i < wordParts.length) {
				//Remove one or more punctuation at the end
				wordParts[i] = wordParts[i].replaceAll("$\\p{Punct}*", "");

				//Only accept the word if it starts with an alpha character (lower or upper)
				m = punct.matcher(wordParts[i]);
				if (m.matches()) {
					context.write(new IntWritable(wordParts[i].length()), new IntWritable(1));
				} //end if m.matches() 
				i++;
			}// end while loop
		} // end map() method
	} //end Mapper class

	
	public static class IntSumReducer extends Reducer<IntWritable,IntWritable,IntWritable,IntWritable> {

		public void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
	      int sum = 0;
	      for (IntWritable val : values) {
	        sum += val.get();
	      }
	      context.write(key, new IntWritable(sum));
		
		}
	}
	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		conf.addResource(new Path("/home/mpcs53013/hadoop/etc/hadoop/core-site.xml"));
		Job job = Job.getInstance(conf, "length count");
		job.setJarByClass(LengthFreq.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setReducerClass(IntSumReducer.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
	
}
	