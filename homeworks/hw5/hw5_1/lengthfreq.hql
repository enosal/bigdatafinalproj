DROP TABLE ss;
DROP TABLE words_split;
DROP TABLE trimmed_words;
DROP TABLE word_lengths;



CREATE TABLE ss (line STRING);
LOAD DATA INPATH '/tmp/input/ss' OVERWRITE INTO TABLE ss;
CREATE TABLE words_split AS
	SELECT word_col FROM 
		(SELECT explode(split(line, '\\s+(\\p{Punct})*')) AS word_col FROM ss) w
 ;

CREATE TABLE trimmed_words AS
 	SELECT regexp_replace(word_col, '(\\p{Punct})*$', '') as word_col	
 	FROM words_split
;
  
CREATE TABLE word_lengths AS
 	SELECT word_col, length(word_col) as len	
 	FROM trimmed_words
;

SELECT length(word_col), count(*) as count FROM trimmed_words 
	GROUP BY length(word_col)
;