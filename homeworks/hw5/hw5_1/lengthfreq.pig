A = load '/tmp/input/ss/*' as line;
B = foreach A generate flatten(TOKENIZE((chararray)$0)) as word;
C = foreach B generate word, SIZE(word) as len;
D = group C by len;
E = foreach D generate COUNT(C) as freq, group as len;
F = order E by len asc;
DUMP F;