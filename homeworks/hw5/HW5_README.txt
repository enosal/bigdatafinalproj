Eryka Nosal
MPCS53013, Autumn 2016
HW5

-------

HW5-1: The results of each of MapReduce, Pig, Hive are textfiles in /hw5_1


/hw5_1/src/main/java/edu.uchicago.erykanosal/LengthFreq.java:
	-Requires 2 arguments: (inputDirectoryLocation outputDirectoryLocation)
	*Output is of the form (word length: word frequency)
	-Repeat words are considered (if there are 6 "the"s, all 6 count towards the length 3 category)

hw5_1/lengthfreq.hql:
	-The table ss reads in the data by line from the given path
	-The table words_split tokenizes the words by splitting on whitespace followed by one or more punctuation
	-The table trimmed_words removes any puncutation from the ends of words and also takes the length
	-The table word_lengths gets the word and its length

	*Output is of the form (word length... word frequency)
	-Repeat words are considered (if there are 6 "the"s, all 6 count towards the length 3 category)
	-Many words with length zero makes me wonder if not all of the whitespace is being caught

hw5_1/lengthfreq.pig:
	-A is a table of lines taken from the data in the input directory
	-B is a table of words from A
	-C is words and their length
	-D is words grouped by their length
	-E is takes the count of how many words per word length
	-F is E but ordered

	-No extra splitting was done (as in mapreduce or hive)
	*Output is of the form (word frequency, word length)





HW5-2: I user the ingestWeather archetype since my submission from hw3_3 would not work when running an hql file on it
	Hive: the results give month 8, day 15, hottest meantemperature 110. Output is in hw5_2weather_maxtemp_results.txt


HW5-3(EC):



HW5-4(EC):