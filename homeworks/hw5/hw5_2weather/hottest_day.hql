ADD JAR /home/mpcs53013/workspace/hw5_2weather/target/uber-hw5_2weather-0.0.1-SNAPSHOT.jar;

DROP TABLE WeatherTable; 

CREATE EXTERNAL TABLE WeatherTable

  ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.thrift.ThriftDeserializer'

  WITH SERDEPROPERTIES (

     'serialization.class' = 'edu.uchicago.erykanosal.weatherSummary.WeatherSummary',

     'serialization.format' =   'org.apache.thrift.protocol.TBinaryProtocol')

  STORED AS SEQUENCEFILE LOCATION '/inputs/thriftWeather';


SELECT month, day, meantemperature	
FROM WeatherTable
ORDER BY meantemperature desc LIMIT 1;
