namespace java edu.uchicago.erykanosal

struct Pedigree {
  1: required i32 true_as_of_secs;
 }
struct Data {
	1: required Pedigree pedigree;
	2: required DataUnit dataunit;
}
union DataUnit {
	1: PersonProp person_prop;
	2: RelationshipEdge rship_edge;
}

struct PersonProp {
	1: required PersonID person_id;
	2: required PersonPropVal person_propval;
}
	
union PersonPropVal {
	1: string name;
	2: Date birthday;
	3: Sex sex;
	4: Location current_town;
	5: Location hometown;
	6: bool activated_profile;
}

union PersonID {
	1: i64 user_id;
}

struct Date {
	1: required byte year;
	2: required byte month;
	3: required byte day;
}

enum Sex {
  MALE = 1,
  FEMALE = 2,
  OTHER = 3
}

struct Location {
	1: optional string city;
	2: optional string state;
	3: optional string country;
} 
	
struct RelationshipEdge {
	1: required PersonID person1;
	2: required PersonID person2;
}

struct PostProperty {
	1: required i32 post_id;
	2: required PersonID author;
	3: required PostPropVal post_propval;
}

union PostPropVal {
	1: Comment comment;
	2: Reaction reaction;
}

struct Reaction {
	1: required i16 reaction_id;
	2: required Expression expression;
}

enum Expression {
	LIKE = 1;
	LOVE = 2;
	SAD = 3;
	ANGRY = 4;
}

struct Comment {
	1: required CommentID comment_id;
	2: required PersonID commentor;
	3: required string comment_content;
}

union CommentID {
	1: i16 comment_id;
}