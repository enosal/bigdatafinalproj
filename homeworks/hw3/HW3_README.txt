Eryka Nosal
MPCS53013
HW3

I am uncertain if my schema is correct from HW, but HW3 is based largely on the schema set up from HW2 (Thrift schema cleaned up slightly since HW2). 


HW3-2: Based off the thrift-example-archetype. The output, to clarify-

	Five facts printed about person 1 (name, sex, active profile, hometown, current town)
	{"1":{"i32":1},"2":{"rec":{"1":{"str":"Kirby"}}}}
	{"1":{"i32":1},"2":{"rec":{"2":{"i32":3}}}}
	{"1":{"i32":1},"2":{"rec":{"5":{"tf":1}}}}
	{"1":{"i32":1},"2":{"rec":{"4":{"rec":{"1":{"str":"Dreamland"}}}}}}
	{"1":{"i32":1},"2":{"rec":{"3":{"rec":{"1":{"str":"Dreamland"}}}}}}

	Four facts printed about person 2 (name, sex, active profile, hometown)
	{"1":{"i32":2},"2":{"rec":{"1":{"str":"Mario"}}}}
	{"1":{"i32":2},"2":{"rec":{"2":{"i32":1}}}}
	{"1":{"i32":2},"2":{"rec":{"5":{"tf":1}}}}
	{"1":{"i32":2},"2":{"rec":{"4":{"rec":{"1":{"str":"Mushroom Kingdom"}}}}}}

	Relationship Edge printed
	{"1":{"i32":1},"2":{"i32":2}}

	
HW3-2HDFS: Based off the hadoop-thrift-example-archetype

	Same example as the non-HDFS except without the relationship edge

HW3-3:

	hw3_3.tgz does not have the data folder in it (not sure why it wasn't included)
	hw3_3.rar does

	Weather data is located in hw3_3/data; two data files are used as an example

	Run GSODThriftSerialization_HW33.java
	Three functions are written:
		1. createDataList loops through data directory, loops reads through the lines of each file, puts them into Data objects
		2. thriftWriter writes out the objects into a thrift file
		3. storeSequenceFile makes the sequence file
		