#!/usr/bin/perl -w
# Creates an html table for census datan

# Needed includes
use strict;
use warnings;
use 5.10.0;
use FindBin;

use Scalar::Util qw(
        blessed
    );
use Try::Tiny;
use Kafka::Connection;
use Kafka::Producer;
use Data::Dumper;
use CGI qw/:standard/, 'Vars';


# Read the state as CGI parameters
my $hhID = param('hhID');
my $yearIntrvw = param('yearIntrvw');
my $monthIntrvw = param('monthIntrvw');
my $state = param('state');
my $famInc = param('famInc');
my $hhType = param('hhType');
my $region = param('region');
my $maritStat = param('maritStat');
my $sex = param('sex');
my $highestEdu = param('highestEdu');

#Restrictions - need an hhID, hhID must be numeric, year of interview must be numeric
if(!$hhID) {
	print header, start_html(-title=>'Add an interview',-head=>Link({-rel=>'stylesheet',-href=>'/erykanosal-table.css',-type=>'text/css'}));
	print table({-class=>'CSS_Table_Example', -style=>'width:80%;'},Tr(), Tr([td(["ERROR: No ID Number provided"])]));
	print end_html;

}
elsif (!($hhID =~ /^\d+?$/)) {
	print header, start_html(-title=>'Add an interview',-head=>Link({-rel=>'stylesheet',-href=>'/erykanosal-table.css',-type=>'text/css'}));
	print table({-class=>'CSS_Table_Example', -style=>'width:80%;'},Tr(), Tr([td(["ERROR: ID number is not a valid entry"])]));
	print end_html;
}
elsif (!($yearIntrvw =~ /^\d+?$/)) {
	print header, start_html(-title=>'Add an interview',-head=>Link({-rel=>'stylesheet',-href=>'/erykanosal-table.css',-type=>'text/css'}));
	print table({-class=>'CSS_Table_Example', -style=>'width:80%;'},Tr(), Tr([td(["ERROR: Year is not a valid entry"])]));
	print end_html;

}
else {

	#Connection
	my ( $connection, $producer );
	try {
	    #-- Connection
	    # $connection = Kafka::Connection->new( host => 'hdp-m.c.mpcs53013-2016.internal', port => 6667 );
	    $connection = Kafka::Connection->new( host => 'localhost', port => 9092 );

	    #-- Producer
	    $producer = Kafka::Producer->new( Connection => $connection );

	    #Message
	    my $message = "<dataPoint><hhID>" . $hhID . "</hhID><interview>";
		$message .= $yearIntrvw 
		. " " . $monthIntrvw 
		. " " . $state 
		. " " . $famInc 
		. " " . $hhType 
		. " " . $region 
		. " " . $maritStat 
		. " " . $sex 
		. " " . $highestEdu ;	
		$message .= "</interview></dataPoint>";

	    # Sending a single message
	    my $response = $producer->send(
		'EN-interview-events',          # topic
		0,                                 # partition
		$message                           # message
		);
	} catch {
	    if ( blessed( $_ ) && $_->isa( 'Kafka::Exception' ) ) {
		warn 'Error: (', $_->code, ') ',  $_->message, "\n";
		exit;
	    } else {
		die $_;
	    }
	};

	# Closes the producer and cleans up
	undef $producer;
	undef $connection;

	print header, start_html(-title=>'Add an interview',-head=>Link({-rel=>'stylesheet',-href=>'/erykanosal-table.css',-type=>'text/css'}));
	print table({-class=>'CSS_Table_Example', -style=>'width:80%;'},
		    caption('Your data has been collected'),
		    Tr(), Tr([td(["Identifier number", $hhID])]));

	#print $protocol->getTransport->getBuffer;
	print end_html;
}

