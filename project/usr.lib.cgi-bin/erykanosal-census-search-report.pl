#!/usr/bin/perl -w
# Creates an html table for census datan

# Needed includes
use strict;
use warnings;
use 5.10.0;
use HBase::JSONRest;
use CGI qw/:standard/;

# Read the state as CGI parameters
my $state = param('state');
 
# Define a connection template to access the HBase REST server
# If you are on our cluster, hadoop-m will resolve to our Hadoop master
# node, which is running the HBase REST server
my $hbase = HBase::JSONRest->new(host => "localhost:8080");
#my $hbase = HBase::JSONRest->new(host => "hdp-m.c.mpcs53013-2016.internal:2056");



sub cellValue {
    my $row = $_[0];
    my $field_name = $_[1];
    my $row_cells = ${$row}{'columns'};
    foreach my $cell (@$row_cells) {
	if ($$cell{'name'} eq $field_name) {
	    return $$cell{'value'};
	}
    }
    return 'missing';
}

# Query hbase for tables
my $table_all = $hbase->get({
  table => 'EN_count_by_state',
  where => {
    key_equals => $state
  },
});

my $table_sex = $hbase->get({
  table => 'EN_sex_count_by_state',
  where => {
    key_begins_with => $state
  },
});

my $table_mstat = $hbase->get({
  table => 'EN_marit_count_by_state',
  where => {
    key_begins_with => $state
  },
});




# Get the rows
my $main_count = @$table_all[0];

my $sex_row1_u = @$table_sex[0];
my $sex_row2_m = @$table_sex[1];
my $sex_row3_f = @$table_sex[2];

my $mstat_row1_n1 = @$table_mstat[0];
my $mstat_row2_1 = @$table_mstat[1];
my $mstat_row3_2 = @$table_mstat[2];
my $mstat_row4_3 = @$table_mstat[3];
my $mstat_row5_4 = @$table_mstat[4];
my $mstat_row6_5 = @$table_mstat[5];
my $mstat_row7_6 = @$table_mstat[6];





#Put in variables
my($tot_responses) =  (cellValue($main_count, 'count:sex'));

my($sex_u_count, $sex_m_count, $sex_f_count)
 =  (cellValue($sex_row1_u, 'state_info:count'),cellValue($sex_row2_m, 'state_info:count'),cellValue($sex_row3_f, 'state_info:count'));

my($mstat_n1_count, $mstat_1_count, $mstat_2_count, $mstat_3_count, $mstat_4_count, $mstat_5_count, $mstat_6_count)
 =  (cellValue($mstat_row1_n1, 'state_info:count'),cellValue($mstat_row2_1, 'state_info:count'),cellValue($mstat_row3_2, 'state_info:count'),cellValue($mstat_row4_3, 'state_info:count'),cellValue($mstat_row5_4, 'state_info:count'),cellValue($mstat_row6_5, 'state_info:count'),cellValue($mstat_row7_6, 'state_info:count'));


#Percentage function
sub percent {
    return sprintf("%.2f", ($_[0])/$tot_responses);
}



# Print an HTML page with the table. Perl CGI has commands for all the
# common HTML tags
print header, start_html(-title=>'State information',-head=>Link({-rel=>'stylesheet',-href=>'/erykanosal-table.css',-type=>'text/css'}));


print p({-style=>"bottom-margin:10px"});
print h1('Report for ' . $state);
print h2('Number of responses');
print table({-class=>'CSS_Table_Example', -style=>'width:60%;margin:auto;'},
	Tr(
		[
			td('Total Respondents'),
			td($tot_responses)
		]
	)),	
    p({-style=>"bottom-margin:10px"});

if ($tot_responses ne 'missing') {
	print h2('By sex');
	print table({-class=>'CSS_Table_Example', -style=>'width:60%;margin:auto;'},
		Tr(
		[	
			td(['Male','Female','Undefined/other']),
			td([$sex_m_count,$sex_f_count,$sex_u_count]),
			td([percent($sex_m_count),percent($sex_f_count),percent($sex_u_count)])
		]
		)),	
	    p({-style=>"bottom-margin:10px"});

	print h2('By marital status');
	print table({-class=>'CSS_Table_Example', -style=>'width:60%;margin:auto;'},
		Tr(
		[	
			td(['No Answer','Married- Spouse Present', 'Married- Spouse absnt', 'Widowed', 'Divorced', 'Separated', 'Never Married']),
			td([$mstat_n1_count, $mstat_1_count, $mstat_2_count, $mstat_3_count, $mstat_4_count, $mstat_5_count, $mstat_6_count]),
			td([percent($mstat_n1_count), percent($mstat_1_count), percent($mstat_2_count), percent($mstat_3_count), percent($mstat_4_count), percent($mstat_5_count), percent($mstat_6_count)])
		]
		)),	
	    p({-style=>"bottom-margin:10px"});



	print h2('Highest Level of Education');
	print h2('Incomes');
}

print end_html;

