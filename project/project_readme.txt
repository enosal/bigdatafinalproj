Eryka Nosal
MPCS53013
Final Project
--------------------------------------------------------
0.About
1.Project Setup
2.Directory Descriptions
3.Get The Data
4.Coding Info
5.Put Data On HDFS
6.Pig: Load Data
7.Pig Queries
8.WebApp
9.WebApp: New Data
10.On Cluster

x.Did Not Use
-------------------------------------------------------


#########
#0.About#
#########

Website is: 
	localhost/erykanosal-census-main.html
	http://104.197.248.161/erykanosal/erykanosal-census-main.html

My project looks at government Census data. 
The webapp allows you to enter a state code and see a count and percentage of various characteristics of that state:
(Only Number total number of responses, sex breakdown, and marital status breakdown per state were implemented)


The webapp also allows you to enter a new "interview" as a datapoint.

################
#1.ProjectSetup#
################
For this readme:
	Run lines with ">>" in the terminal
	Run lines with "**" as java applications

Make sure:
	start_big_data_services.sh
		(start-dfs.sh)
		(start-yarn.sh)
		(start-hbase.sh)
	sudo service apache2 start
	hbase rest server
	hdfs dfsadmin -safemode leave

#########################
#2.DirectoryDescriptions#
#########################
Local Directories:
	pig_scripts
	data
	data/codes
Local Files:
	variables.txt
	project_readme.txt
	
HDFS Directories:
	/erykanosal/inputData
		serialize from raw downloaded data using SerializeCensusSummary.java
	/erykanosal/codesData
		copy codes from local files
	/erykanosal/codesDataPig 
		after loadState.pig has been run	

##############
#3.GetTheData#
##############
Sample datasets have been retrieved already and stored in data/ folder

Data.gov page: 
	https://catalog.data.gov/dataset/current-population-survey-annual-social-and-economic-supplement/resource/4ab71894-5790-495d-a6c8-0e9a7647dac8
Key: 
	http://thedataweb.rm.census.gov/pub/cps/basic/201501-/January_2015_Record_Layout.txt
Data: 
	Under Basic Monthly CPS. 



##############
#4.CodingInfo#
##############
Coding of the data:
	see variables.txt
	see codes/

#################
#5.PutDataOnHDFS#
#################
Create directories:
	>>hdfs dfs -mkdir /erykanosal
	>>hdfs dfs -mkdir /erykanosal/inputData
	>>hdfs dfs -mkdir /erykanosal/codesData

Census Data on HDFS:
	**Run SerializeCensusSummary.java with argument "/home/mpcs53013/workspace/erykanosal_final/data"

Code Data on HDFS:
	>>hdfs dfs -copyFromLocal	/home/mpcs53013/workspace/erykanosal_final/data/codes/state.txt /erykanosal/codesData/state.txt


################
#6.Pig:LoadData#
################
Loads:
	>>pig pig_scripts/loadState.pig
	>>pig pig_scripts/loadCensus.pig
Joins on state:
	>>pig pig_scripts/joinState.pig



##############
#7.PigQueries#
##############
>>hbase shell erykanosal_hbase_create_tables.txt
>>pig pig_scripts/groupByState.pig
>>pig pig_scripts/countSex.pig
>>pig pig_scripts/countMStat.pig


##########
#8.WebApp#
##########
Perl Scripts: /usr/lib/cgi-bin/
	erykanosal-census-add-report.pl
	erykanosal-census-search-report.pl

HTML Files: /var/www/html/
	erykanosal-census-main.html
	erykanosal-census-search.html
	erykanosal-census-add.html

CSS Files: /var/www/html
	erykanosal-table.css
	erykanosal-white-pink.css

(Speed layer) Kafka Topic:
	EN-interview-events

(Speed layer) Hbase Table:
	EN_new_interviews

##################
#9.WebApp:NewData#
##################
>>storm nimbus
>>storm supervisor
>>storm ui
>>kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic EN-interview-events
>>kafka-console-consumer.sh --zookeeper localhost:2181 --topic EN-interview-events
**Run CensusTopology.java

Submit new interview on the webapp

>>hbase shell
	>>scan 'EN_new_interviews', {'LIMIT'=>5}

##############
#10.OnCluster#
##############
webserver
	Perl Scripts: /usr/lib/cgi-bin/erykanosal
	HTML&CSS Scripts: /var/www/html/erykanosal

hdp-m
	Already done
		>>hdfs dfs -mkdir /erykanosal
		>>hdfs dfs -mkdir /erykanosal/inputData
		>>hdfs dfs -mkdir /erykanosal/codesData
		>>hdfs dfs -copyFromLocal /home/mpcs53013/erykanosal/final_project/codes/state.txt /erykanosal/codesData/state.txt
		>>yarn jar ~/erykanosal/final_project/uber-erykanosal_final-0.0.1-SNAPSHOT.jar edu/uchicago/erykanosal/erykanosal_final/SerializeCensusSummary /home/mpcs53013/erykanosal/final_project/data
		>>pig ~/erykanosal/final_project/pig_scripts/loadState.pig
		>>pig ~/erykanosal/final_project/pig_scripts/loadCensus.pig
		>>pig ~/erykanosal/final_project/pig_scripts/joinState.pig
		>>hbase shell ~/erykanosal/final_project/erykanosal_hbase_create_tables.txt
		>>pig ~/erykanosal/final_project/pig_scripts/groupByState.pig
		>>pig ~/erykanosal/final_project/pig_scripts/countSex.pig
		>>pig ~/erykanosal/final_project/pig_scripts/countMStat.pig
	
	Now can view search!

	To add new data
		already done (>>kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic EN-interview-events)

		>>storm jar -c zookeeper.znode.parent=/hbase-unsecure ~/erykanosal/final_project/uber-censusTopology-0.0.1-SNAPSHOT.jar edu.uchicago.erykanosal.censusTopology.CensusTopology ENCensusTopology
		>>kafka-console-consumer.sh --zookeeper kafka-console-consumer.sh --zookeeper localhost:2181 --topic EN-interview-events
		
		>>hbase shell
			>>scan 'EN_new_interviews', {'LIMIT'=>5}








#############
#x.DidNotUse#
#############
12/8/16
	--Census Data--
		No queries for Income or Highest Level of Education

	--Voting Data--
	Data.gov page:
		https://catalog.data.gov/dataset/current-population-survey-voting-and-registration-supplement
	Key: starting at page 21
		http://www2.census.gov/programs-surveys/cps/techdocs/cpsnov14.pdf
	Data:
		Under CPS Supplement. I selected November 2014 Voting and Registration Data File
